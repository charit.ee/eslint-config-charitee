[![npm version](https://badge.fury.io/js/eslint-config-charitee.svg)](https://badge.fury.io/js/eslint-config-charitee)

Charit.ee Inc Styleguide
========================

* Some rule overrides for the great `eslint-config-airbnb` package.
