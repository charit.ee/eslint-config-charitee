module.exports = {
  extends: [
    'eslint-config-airbnb-base',
    './overrides/'
  ].map(require.resolve),
  plugins: [
    "mocha",
    "promise",
    "async-await"
  ],
  eslintConfig: {
    parser: 'babel-eslint',
  },
  env: {
    mocha: true
  }
};
