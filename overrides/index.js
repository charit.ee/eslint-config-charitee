module.exports = {
  rules: {
    "no-multi-spaces": ["error", {
      "exceptions": {
        "Property": true,
        "VariableDeclarator": true,
        "ImportDeclaration": true
      }
    }],
    "comma-dangle": ["error", {
      "functions": "never",
      "arrays": "always-multiline"
    }],
    "key-spacing": ["error", {
      "align": "colon"
    }],
    "no-param-reassign": ["error", {
      "props": false
    }],
    "arrow-parens": [2, "as-needed", {
      "requireForBlockBody": false
    }],
    "no-unused-expressions": [
      "error",
      {
        "allowTernary": true
      }
    ],
    "no-use-before-define": 0,
    "arrow-body-style": 0,
    "dot-notation": 0,
    "no-console": 0,
    "no-param-reassign": [
      "error",
      {
        "props": false
      }
    ],
    "comma-dangle": [
      "warn",
      "only-multiline"
    ],
    "no-multi-spaces": [
      "error",
      {
        "exceptions": {
          "ImportDeclaration": true,
          "VariableDeclarator": true
        }
      }
    ],
    "one-var": 0,
    "no-var": 0,
    "no-mixed-operators": [
      "error",
      {
        "allowSamePrecedence": true
      }
    ],
    "max-len": ["warn", {
      code: 100,
      tabWidth: 2,
      ignoreTrailingComments: true,
      ignoreTemplateLiterals: true,
      ignoreStrings: true
    }],
    "no-plusplus": ["error", {
      allowForLoopAfterthoughts: true
    }],
    "no-warning-comments": ["warn", {
      // terms: [],
      // location: "start"
    }],
  }
}
